module Krivine

%default total

data Ty : Type where
  TyAt : Ty
  TyFn : (u, v : Ty) -> Ty

data Basis : Type where
  Nil  : Basis
  (::) : (u : Ty) -> (g : Basis) -> Basis

data Vr : (g : Basis) -> (u : Ty) -> Type where
  VZ : Vr (u :: g) u
  VS : (x : Vr g u) -> Vr (v :: g) u

data Tm : (g : Basis) -> (u : Ty) -> Type where
  App : (t1 : Tm g (TyFn u v)) -> (t2 : Tm g u) -> Tm g v
  Lam : (t0 : Tm (u :: g) v) -> Tm g (TyFn u v)
  Var : (x : Vr g u) -> Tm g u

TmTy : Type
TmTy = Basis -> Ty -> Type

using (T : TmTy)

  data Subst : (T : TmTy) -> (g1, g2 : Basis) -> Type where
    Comp  : (s1 : Subst T g1 g2) -> (s2 : Subst T g2 g3) -> Subst T g1 g3
    Cons  : (t : T g2 u) -> (s : Subst T g1 g2) -> Subst T (u :: g1) g2
    Id    : Subst T g g
    Lift  : (s : Subst T g1 g2) -> Subst T (u :: g1) (u :: g2)
    Shift : Subst T g (u :: g)

data Thunk : TmTy where
  Th : (t : Tm g1 u) -> (s0 : Subst Thunk g1 g2) -> Thunk g2 u

boxvar : Vr g u -> Thunk g u
boxvar x = Th (Var x) Id

close : Thunk g1 u -> Subst Thunk g1 g2 -> Thunk g2 u
close (Th t s0) s = Th t (Comp s0 s)

lookup : (x : Vr g1 u) -> (s : Subst Thunk g1 g2) -> Thunk g2 u
lookup x (Comp s1 s2) = close (lookup x s1) s2
lookup VZ (Cons t s) = t
lookup (VS x) (Cons t s) = lookup x s
lookup x Id = boxvar x
lookup VZ (Lift s) = boxvar VZ
lookup (VS x) (Lift s) = close (lookup x s) Shift
lookup x Shift = boxvar (VS x)

mutual

  partial
  subst : Tm g1 u -> Subst Thunk g1 g2 -> Tm g2 u
  subst (App t1 t2) s = App (subst t1 s) (subst t2 s)
  subst (Lam t0) s = Lam (subst t0 (Lift s))
  subst (Var x) Id = Var x
  subst (Var x) s = force (lookup x s)

  partial
  force : Thunk g u -> Tm g u
  force (Th t s0) = subst t s0

using (T : TmTy)

  data Context : (T : TmTy) -> (g1, g2 : Basis) -> (u, v : Ty) -> Type where
    App1 : (c : Context T g1 g2 v w) -> (t0 : T g1 u) ->
              Context T g1 g2 (TyFn u v) w
    App2 : (t0 : T g1 (TyFn u v)) -> (c : Context T g1 g2 v w) ->
              Context T g1 g2 u w
    Lam1 : (c : Context T g1 g2 (TyFn u v) w) ->
              Context T (u :: g1) g2 v w
    Top  : Context T g g w w

  foldctx : ({g : Basis} -> {u : Ty} -> T g u -> Tm g u) ->
            Tm g1 v -> Context T g1 g2 v w -> Tm g2 w
  foldctx f t (App1 c t0) = foldctx f (App t (f t0)) c
  foldctx f t (App2 t0 c) = foldctx f (App (f t0) t) c
  foldctx f t (Lam1 c) = foldctx f (Lam t) c
  foldctx f t Top = t

data Head : (g : Basis) -> Ty -> Type where
  MkHead : Vr g w -> Head g w

using (T : TmTy)

  data Spine : (T : TmTy) -> (g : Basis) -> (w : Ty) -> Type where
    MkSpine : Head g1 u -> Context T g1 g2 u w -> Spine T g2 w

partial
eval : (t : Thunk g1 u) -> (c : Context Thunk g1 g2 u w) ->
          Spine Thunk g2 w
eval (Th (App t1 t2) s0) c =
  eval (Th t1 s0) (App1 c (Th t2 s0))
eval (Th (Lam t0) s0) (App1 c t1) =
  eval (Th t0 (Cons t1 s0)) c
eval (Th (Lam t0) s0) c =
  eval (Th t0 (Lift s0)) (Lam1 c)
eval (Th (Var x) Id) c =
  MkSpine (MkHead x) c
eval (Th (Var x) s0) c =
  eval (lookup x s0) c

mutual

  data Subst' : (g1, g2 : Basis) -> Type where
    Comp'  : (s1 : Subst Thunk' g1 g2) -> (s2 : Subst' g2 g3) -> Subst' g1 g3
    Id'    : Subst' g g

  data Thunk' : TmTy where
    Th' : (t : Tm g1 u) -> (s0 : Subst' g1 g2) -> Thunk' g2 u

data Machine : TmTy where
  MkM : (t : Thunk' g1 u) -> (c : Context Thunk' g1 g2 u w) ->
    Machine g2 w

coerce : Subst' g1 g2 -> Subst Thunk' g1 g2
coerce (Comp' s1 s2) = Comp s1 (coerce s2)
coerce Id' = Id

step : (m : Machine g u) -> Either (Spine Thunk' g u) (Machine g u)
step (MkM (Th' (Var x) (Comp' Shift s)) c) =
  Right (MkM (Th' (Var (VS x)) s) c)
step (MkM (Th' (Var VZ) (Comp' (Cons (Th' t s1) _) s2)) c) =
  Right (MkM (Th' t (Comp' (coerce s1) s2)) c)
step (MkM (Th' (Var VZ) (Comp' (Lift _) s)) c) =
  Right (MkM (Th' (Var VZ) s) c)
step (MkM (Th' (Var (VS x)) (Comp' (Cons (Th' _ _) s1) s2)) c) =
  Right (MkM (Th' (Var x) (Comp' s1 s2)) c)
step (MkM (Th' (Var (VS x)) (Comp' (Lift s1) s2)) c) =
  Right (MkM (Th' (Var x) (Comp' s1 (Comp' Shift s2))) c)
step (MkM (Th' t (Comp' (Comp s1 s2) s3)) c) =
  Right (MkM (Th' t (Comp' s1 (Comp' s2 s3))) c)
step (MkM (Th' t (Comp' Id s)) c) =
  Right (MkM (Th' t s) c)
step (MkM (Th' t1 s1) (App2 (Th' t2 s2) c)) =
  Right (MkM (Th' t2 s2) (App1 c (Th' t1 s1)))
step (MkM (Th' (App t (Var x)) s) c) =
  Right (MkM (Th' (Var x) s) (App2 (Th' t s) c))
step (MkM (Th' (App t1 t2) s) c) =
  Right (MkM (Th' t1 s) (App1 c (Th' t2 s)))
step (MkM (Th' (Lam t1) s) (App1 c t2)) =
  Right (MkM (Th' t1 (Comp' (Cons t2 (coerce s)) Id')) c)
step (MkM (Th' (Lam t) s) c) =
  Right (MkM (Th' t (Comp' (Lift (coerce s)) Id')) (Lam1 c))
step (MkM (Th' (Var x) Id') c) =
  Left (MkSpine (MkHead x) c)

partial
reduce : (m : Machine g u) -> Spine Thunk' g u
reduce m with (step m)
  reduce m | Left sp = sp
  reduce m | Right m' = reduce m'
